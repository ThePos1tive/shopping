# @... don't print commmand to terminal
dockexec = @docker-compose exec backend python manage.py

shell:
	${dockexec} shell

test:
	${dockexec} test

makemigrations:
	${dockexec} makemigrations

migrate:
	${dockexec} migrate

createsuperuser:
	${dockexec} createsuperuser

backbash:
	@docker-compose exec backend bash

frontbash:
	@docker-compose exec frontend bash
