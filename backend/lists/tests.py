from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status

from lists.models import ShoppingList


class PrivateShoppintListApiTests(TestCase):
    """Test for /lists/* endpoint"""

    def obtain_token(self, credentials):
        """Obtains jwt token (helper method)"""
        response = self.client.post('/auth/token/obtain', credentials)
        return response.data['access']

    def create_shopping_list(self, name):
        """Creates a new shopping list (helper method)"""
        return ShoppingList.objects.create(name=name, owner=self.user)

    def create_user(self, credentials):
        """Creates a user (helper method)"""
        return User.objects.create_user(**credentials)

    def setUp(self):
        self.url = '/lists/'
        self.user_credentials = {
            'email': 'root@gmail.com',
            'username': 'root@gmail.com',
            'password': 'root'
        }
        # default user for testing
        self.user = self.create_user(self.user_credentials)
        self.client = APIClient()
        # get jwt token for authentication (login)
        access_token = self.obtain_token(self.user_credentials)
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')

    def test_get_all_shoppint_lists(self):
        """Test getting ('GET') all shopping list for current user
        is successful
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # result should contains number of all shopping lists (count)
        # and list with shopping lists (result)
        self.assertIn('count', response.data)
        self.assertIn('results', response.data)

    def test_create_shopping_list_with_valid_name(self):
        """Test creating ('POST') a shopping list with valid data
        (length<=30) is successful
        """
        data = {'name': 'test_shoppint_list'}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        self.assertIn('name', response.data)
        self.assertIn('items', response.data)
        # newly created shopping list should not contain items
        self.assertEqual(0, len(response.data['items']))

    def test_create_shopping_list_with_too_long_name(self):
        """Test creating ('POST') a shopping list with too long name
        (length>30) is failed
        """
        data = {'name': 'test_shopping_list_with_too_long_name \
                            longer_than_you_think'}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # response should contain error message with description
        self.assertIn('name', response.data)

    def test_remove_existing_shopping_list(self):
        """Test removing a shopping list is successful"""
        shopping_list = self.create_shopping_list(name='test_shopping_list')
        response = self.client.delete(f'{self.url}{shopping_list.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_remove_nonexistent_shopping_list(self):
        """Testing removing a shopping list that does not exist is failed"""
        response = self.client.delete(f'{self.url}1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_access_to_list(self):
        """Test getting access to a shopping list that does not belong to
        current user is failed
        """
        shopping_list = self.create_shopping_list(name='test_shopping_list')
        different_user_credentials = {
            'username': 'test@gmail.com',
            'email': 'test@gmail.com',
            'password': 'test'
        }
        self.create_user(different_user_credentials)
        access_token = self.obtain_token(different_user_credentials)
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.delete(f'{self.url}{shopping_list.id}/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
