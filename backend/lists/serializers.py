from rest_framework import serializers

from lists.models import ShoppingList, ShoppingItem


class ShoppingItemSerializer(serializers.ModelSerializer):
    """Serializer for ShoppingItem model"""
    class Meta:
        model = ShoppingItem
        fields = ('id', 'name', 'is_done')


class ShoppingListListSerializer(serializers.ModelSerializer):
    """
    Serializer for ShoppingList model (list view only), to not
    userload the response with nested serializers
    """
    class Meta:
        model = ShoppingList
        fields = ('id', 'name', 'is_finished')
        read_only_fields = ('id', 'name', 'is_finished')


class ShoppingListDetailSerializer(serializers.ModelSerializer):
    """Detail serializer for ShoppingList model"""
    items = ShoppingItemSerializer(many=True, required=False)

    class Meta:
        model = ShoppingList
        fields = ('id', 'name', 'items')
        read_only_fields = ('is_finished',)

    def create(self, validated_data):
        """
        By default ModelSerializer does not support nested serializers,
        so create and update method should be overwritten to support them
        """
        items_data = validated_data.pop('items', None)
        if items_data:
            shopping_list = ShoppingList.objects.create(**validated_data)
            if not items_data:
                return shopping_list
            for item_data in items_data:
                shopping_list.items.create(**item_data)
            return shopping_list
        return ShoppingList.objects.create(**validated_data)

    def create_or_update_shopping_items(self, instance, shopping_items_data):
        """Updates shopping items or create new if do not exist"""
        for item_data in shopping_items_data:
            pk = item_data.pop('id', None)
            item = instance.items.filter(pk=pk).first()
            if item:
                # update an existing shopping item
                # item.update(**item_data) - does not work with one item
                item.name = item_data.get('name', item.name)
                item.is_done = item_data.get('is_done', item.is_done)
                item.save()
            else:
                instance.items.create(**item_data)

    def remove_items_from_shopping_list(self, instance, items_ids: list):
        """Removes shopping items from a shopping list"""
        if items_ids:
            instance.items.filter(pk__in=items_ids).delete()

    def is_finished(self, instance):
        """Checks if shopping list is finished"""
        # If all items in a shopping list are done,
        # the list should be marked as finished
        number_of_all = instance.items.count()
        number_of_done = instance.items.filter(is_done=True).count()
        return number_of_all == number_of_done

    def update(self, instance, validated_data):
        """Updates shopping list in a shopping list"""
        shopping_items_data = validated_data.pop('items')
        self.create_or_update_shopping_items(instance, shopping_items_data)
        self.remove_items_from_shopping_list(
            instance,
            self.initial_data.get('removedItems')
        )
        instance.is_finished = self.is_finished(instance)
        instance.save()
        return instance
