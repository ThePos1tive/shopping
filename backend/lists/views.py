from rest_framework import viewsets, permissions, status
from rest_framework.response import Response

from lists.serializers import (
    ShoppingListDetailSerializer,
    ShoppingListListSerializer
)
from lists.permissions import IsOwner


class ShoppingListViewSet(viewsets.ModelViewSet):
    serializer_class = ShoppingListDetailSerializer
    # this is separated serializer for 'list' action (view)
    # # to not overload response with items when is is not required
    list_serializer_class = ShoppingListListSerializer
    # only owner can get access to a list
    # only authenticated user can create a list
    permission_classes = [permissions.IsAuthenticated, IsOwner, ]

    def get_serializer_class(self):
        """Determins which serializer to use 'list' or 'detail'"""
        if self.action == 'list':
            if hasattr(self, 'list_serializer_class'):
                return self.list_serializer_class
        return super().get_serializer_class()

    def create(self, request):
        """POST method, create a list and add owner"""
        serializer = ShoppingListDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # add owner to a list
        serializer.save(owner=request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)

    def partial_update(self, request, *args, **kwargs):
        """PATCH method, partial update"""
        instance = self.get_object()
        # name of the shopping list is not changeable
        # and it does not send in the request.data
        # so it should be added default value
        request.data['name'] = instance.name
        serializer = ShoppingListDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update(instance, request.data)
        return Response(serializer.data, status.HTTP_200_OK)

    def get_queryset(self):
        """Queryset contains only current user's lists"""
        return self.request.user.lists.all()
