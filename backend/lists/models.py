from django.db import models
from django.contrib.auth.models import User


class ShoppingList(models.Model):
    """List that contains shopping items"""
    name = models.CharField(max_length=30)
    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='lists')
    is_finished = models.BooleanField(default=False)

    class Meta:
        ordering = ('id',)


class ShoppingItem(models.Model):
    """Item in a shopping list"""
    name = models.CharField(max_length=30)
    is_done = models.BooleanField(default=False)
    shopping_list = models.ForeignKey(ShoppingList, on_delete=models.CASCADE,
                                      related_name='items')

    class Meta:
        ordering = ('id',)
