from rest_framework.routers import DefaultRouter

from lists.views import ShoppingListViewSet


router = DefaultRouter()
router.register(r'', ShoppingListViewSet, basename='list')


urlpatterns = router.urls
