from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """Only owner is allowed to access the object"""
    def has_object_permission(self, request, view, obj):
        """Returns True if the current user is the owner"""
        return request.user == obj.owner
