from random import randint, choice
from django.contrib.auth.models import User
from lists.models import ShoppingList


def generate():
    user = User.objects.get(pk=1)
    for i in range(50):
        shopping_list = ShoppingList(name=f'Test_name{i}', owner=user)
        shopping_list.save()
        for j in range(randint(0, 20)):
            shopping_list.items.create(name=f'TestItem{j}',
                                       is_done=choice([True, False]))
    return True
