from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status


def create_user(credentials):
    """Create user helper method"""
    User.objects.create_user(**credentials).save()


class AuthJWTApiTests(TestCase):
    """Tests for /auth/token/obtain, /auth/token/verify endpoints"""
    # obtain = signin = login

    def obtain_token(self, credentials):
        """Obtains jwt token helper method"""
        response = self.client.post('/auth/token/obtain', credentials)
        return response.data['access']

    def setUp(self):
        self.client = APIClient()
        self.user_credentials = {
            'email': 'root@gmail.com',
            'username': 'root@gmail.com',
            'password': 'root'
        }
        create_user(self.user_credentials)
        self.access_token = self.obtain_token(self.user_credentials)

    def test_obtain_token_with_valid_data(self):
        """Test obtaining a token with valid credentials is successful"""
        response = self.client.post('/auth/token/obtain', self.user_credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data)
        self.assertTrue('refresh', response.data)

    def test_obtain_token_with_invalid_data(self):
        """Test obtaining a token with invalid credentials is failed"""
        credentials = {'username': 'admin@gmail.com', 'password': 'root'}
        response = self.client.post('/auth/token/obtain', credentials)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('access', response.data)
        self.assertNotIn('refresh', response.data)

    def test_verify_token_with_valid_data(self):
        """Test verifying a token with valid access token is successful"""
        data = {'token': self.access_token}
        response = self.client.post('/auth/token/verify', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_token_with_invalid_data(self):
        """Test verifying a token with invalid access token is failed"""
        data = {'token': 'randome_string'}
        response = self.client.post('/auth/token/verify', data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class RegistationUserApiTests(TestCase):
    """Tests for /auth/accounts endpoint"""

    def setUp(self):
        self.client = APIClient()
        self.url = '/auth/accounts'

    def test_create_user_with_valid_data(self):
        """Test creating a new user with valid data is successful"""
        data = {'email': 'root@gmail.com', 'password': 'root'}
        response = self.client.post(self.url, data)
        user = User.objects.get(**response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotIn('password', response.data)
        self.assertTrue(user.check_password(data['password']))

    def test_create_user_exists(self):
        """Test creating a new user that already exists is failed"""
        data = {'email': 'root@gmail.com', 'password': 'root'}
        # create the first time
        self.client.post(self.url, data)
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_with_empty_credentials(self):
        """Test creating a new user with empty fields (credentials) is failed"""
        data = {'email': '  ', 'password': '  '}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # response contains error messages for each field
        self.assertIn('email', response.data)
        self.assertIn('password', response.data)

    def test_create_user_with_invalid_email(self):
        """Test creating a new user with invalid email (without @) is failed"""
        data = {'email': 'rootgmail.com', 'password': 'root'}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('email', response.data)
        # password is OK
        self.assertNotIn('password', response.data)

    def test_create_user_with_too_long_email(self):
        """Test creating a new user with too long email (>50) is failed"""
        data = {
            'email': 'itisanewuserwithlongemailitisevenlongerthanyouthink@gmail.com',
            'password': 'root'
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # response contains field email with error message
        self.assertIn('email', response.data)
        # response does not contain field password because there is no error
        self.assertNotIn('password', response.data)

    def test_create_user_with_too_long_password(self):
        """Test creating a new user with too long password (>50) is failed"""
        data = {
            'email': 'root@gmail.com',
            'password': 'itisanewuserwithlongemailitisevenlongerthanyouthink'
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # response contains field password with error message
        self.assertIn('password', response.data)
        # response does not contain field email because there is no error
        self.assertNotIn('email', response.data)
