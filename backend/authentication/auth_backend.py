from django.contrib.auth.models import User


class CustomAuthenticationBackend:
    """
    Custom authentication that allowes to use credentials
    email + password instead of username + password
    BUT: should always use 'username' field with email inside for login
    """
    def authenticate(self, request, username=None, password=None):
        """Returns user object if it exists
        and has a valid password, otherwise None
        """
        try:
            user = User.objects.get(email=username)
        except User.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None

    def get_user(self, user_id):
        """Return user when it is requested (all requests)"""
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
