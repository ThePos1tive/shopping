from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from rest_framework import status
from rest_framework import permissions

from authentication.serializers import UserSerializer


class UserCreateView(CreateAPIView):
    """Create (register account) user view"""

    # every one is allowed to create an account
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format='json'):
        """Creates a new user (account)"""
        serializer = UserSerializer(data=request.data)
        # look for a user with this email (checking, if it do not exist)
        user = User.objects.filter(email=request.data['email'])
        # if user with current email does not exist
        if not user:
            if serializer.is_valid():
                user = serializer.save()
                if user:
                    return Response({'id': user.id, 'email': user.email},
                                    status=status.HTTP_201_CREATED)
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response({'detail': 'User with the given email already exists'},
                        status=status.HTTP_400_BAD_REQUEST)
