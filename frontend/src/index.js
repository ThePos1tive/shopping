import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line import/extensions
import App from './App.jsx';
import './style.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(React.createElement(App, {}, null), document.getElementById('root'));

serviceWorker.unregister();
