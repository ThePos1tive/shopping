import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import Signin from './components/authentication/Signin';
import Signup from './components/authentication/Signup';
import Logout from './components/authentication/Logout';
import ErrorPage from './components/common/Error';
import ShoppingList from './components/shopping_list/ShoppingList';
import ShoppingLists from './components/shopping_lists/ShoppingLists';
import { AuthProvider } from './Context';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: true,
    };
    this.isAuthenticated = this.isAuthenticated.bind(this);
  }

  componentWillMount() {
    this.isAuthenticated();
  }

  handleLogout = () => {
    this.setState({
      isAuthenticated: false,
    });
    localStorage.removeItem('shopping_access_token');
  }

  handleLogin = (accessToken) => {
    this.setState({
      isAuthenticated: true,
    });
    localStorage.setItem('shopping_access_token', accessToken);
  }

  async isAuthenticated() {
    /* checks if current token is valid (or is not expired) */
    const accessToken = localStorage.getItem('shopping_access_token');
    if (!accessToken) {
      this.setState({
        isAuthenticated: false,
      });
    }
    const body = {
      token: accessToken,
    };
    const response = await fetch('http://localhost:8000/auth/token/verify', {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(body),
    });
    this.setState({
      isAuthenticated: response.status === 200,
    });
  }

  render() {
    const { isAuthenticated } = this.state;
    const context = {
      isAuthenticated,
      handleLogin: this.handleLogin,
      handleLogout: this.handleLogout,
    };
    return (
      <AuthProvider value={context}>
        <Router>
          <Navbar isAuthenticated={isAuthenticated} handleLogout={this.handleLogout} />
          <div className="container">
            <br />
            <Switch>
              <Route exact path="/" component={ShoppingLists} />
              <Route path="/signin" component={Signin} />
              <Route path="/signup" component={Signup} />
              <Route path="/logout" component={Logout} />
              <Route path="/lists/:id" component={ShoppingList} />
              <Route component={ErrorPage} />
            </Switch>
          </div>
        </Router>
      </AuthProvider>
    );
  }
}

export default App;
