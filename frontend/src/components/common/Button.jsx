import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ text, ...rest }) => (
  <button type="submit" className="btn btn-primary btn-raised" {...rest}>{text}</button>
);

Button.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Button;
