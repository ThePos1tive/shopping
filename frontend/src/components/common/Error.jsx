import React from 'react';
import PropTypes from 'prop-types';

const ErrorPage = ({ code, message }) => (
  <div className="d-flex justify-content-center align-items-center" style={{ marginTop: '25vh' }}>
    <h1 className="mr-3 pr-3 align-top border-right inline-block align-content-center">
      {code}
    </h1>
    <div className="inline-block align-middle">
      <h2 className="font-weight-normal lead" id="desc">
        {message}
      </h2>
    </div>
  </div>
);

ErrorPage.propTypes = {
  code: PropTypes.number,
  message: PropTypes.string,
};

// by default it is 404 error
ErrorPage.defaultProps = {
  code: 404,
  message: 'The page you requested was not found.',
};

export default ErrorPage;
