import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ id, placeholder, type, errorMessage, handleChange, value }) => (
  // here should add ...rest and import in to the input element
  <div className="form-group">
    <label className="bmd-label-static" htmlFor={id}>
      {placeholder}
    </label>
    <input
      type={type}
      className="form-control"
      id={id}
      value={value}
      aria-describedby="helper"
      placeholder={placeholder}
      onChange={handleChange}
    />
    <small id="helper" className="form-text text-muted">
      {errorMessage}
    </small>
  </div>
);

Input.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string,
  errorMessage: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
};

Input.defaultProps = {
  type: 'text',
  errorMessage: '',
};

export default Input;
