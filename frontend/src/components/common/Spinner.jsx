import React from 'react';
import { ClipLoader } from 'react-spinners';

const Spinner = () => (
  <div className="sweet-loading text-center" style={{ marginTop: '25vh' }}>
    <ClipLoader
      sizeUnit="px"
      size={50}
      color="#2CE2BD"
      loading
    />
  </div>
);

export default Spinner;
