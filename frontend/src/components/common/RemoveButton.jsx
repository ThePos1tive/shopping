import React from 'react';
import PropTypes from 'prop-types';

const RemoveButton = ({ handleShowConfirmationPopUp }) => (
  <div className="label label-default label-pill pull-xs-right">
    <button
      type="button"
      onClick={handleShowConfirmationPopUp}
      className="btn btn-primary btn-sm"
      style={{ margin: '0', padding: '0' }}
    >
      <i className="material-icons">delete</i>
    </button>
  </div>
);

RemoveButton.propTypes = {
  handleShowConfirmationPopUp: PropTypes.func.isRequired,
};

export default RemoveButton;
