import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const NavItem = ({ text, url }) => (
  <li className="nav-item">
    <Link to={url} className="nav-link">
      {text}
    </Link>
  </li>
);

NavItem.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default NavItem;
