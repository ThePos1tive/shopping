import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import NavItem from './NavItem';

const Navbar = ({ isAuthenticated }) => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <Link to="/" className="navbar-brand">
      Shopping
    </Link>
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarNav"
      aria-controls="navbarNav"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav mr-auto">
        {/* Something */}
      </ul>
      <div className="form-inline my-2 my-lg-0">
        <ul className="navbar-nav mr-auto">
          {isAuthenticated ? (
            <NavItem text="Logout" url="/logout" />
          ) : (
            <>
              <NavItem text="Sign in" url="/signin" />
              <NavItem text="Sign up" url="/signup" />
            </>
          )}
        </ul>
      </div>
    </div>
  </nav>
);

Navbar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

export default Navbar;
