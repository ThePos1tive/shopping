import React from 'react';
import { Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import Input from '../common/Input';
import Button from '../common/Button';
import AuthContext from '../../Context';

class Signup extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailError: '',
      passwordError: '',
      redirectToSignin: false,
    };
    this.handleSignup = this.handleSignup.bind(this);
  }

  showPopUp = (title, message, redirect) => {
    confirmAlert({
      title,
      message,
      buttons: [
        {
          label: 'Ok',
          onClick: () => {
            this.setState({
              redirectToSignin: redirect,
            });
          },
        },
      ],
    });
  };

  handleChange = (event) => {
    // make inputs controlled components
    const { id, value } = event.target;
    // error name for a particular field
    const error = `${id}Error`;
    if (value.length > 50) {
      this.setState({
        [id]: value,
        [error]: 'Maximum length of this field – 50 characters',
      });
    } else {
      this.setState({
        [id]: value,
        [error]: '',
      });
    }
  };

  async handleSignup(event) {
    // do not refresh a page
    event.preventDefault();
    const { email, password, emailError, passwordError } = this.state;

    // if there is an error, do not sign in
    if (emailError || passwordError) {
      return;
    }

    const credentials = {
      email,
      password,
    };

    const response = await fetch('http://localhost:8000/auth/accounts', {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(credentials),
    });

    // if is created
    if (response.status === 201) {
      this.showPopUp('Success', 'Using your credentials, you can now sign in.', true);
    } else {
      this.showPopUp(
        'Error',
        'Invalid email or password or user exists.',
        false,
      );
    }
  }

  render() {
    const { email, emailError, password, passwordError, redirectToSignin } = this.state;
    const { isAuthenticated } = this.context;
    if (isAuthenticated) return <Redirect to="/" />;
    if (redirectToSignin) {
      return <Redirect to="/signin" />;
    }
    return (
      <form onSubmit={this.handleSignup}>
        <h1>Sign up</h1>
        <Input
          id="email"
          value={email}
          placeholder="Enter email"
          type="email"
          errorMessage={emailError}
          handleChange={this.handleChange}
        />
        <Input
          id="password"
          value={password}
          placeholder="Enter password"
          type="password"
          errorMessage={passwordError}
          handleChange={this.handleChange}
        />
        <Button text="Create an account" />
      </form>
    );
  }
}

export default Signup;
