import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import AuthContext from '../../Context';

const Logout = () => {
  const authContext = useContext(AuthContext);
  authContext.handleLogout();
  return <Redirect to="/signin" />;
};

export default Logout;
