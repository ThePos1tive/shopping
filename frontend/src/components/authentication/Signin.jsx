import React from 'react';
import { Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import Input from '../common/Input';
import Button from '../common/Button';
import AuthContext from '../../Context';

class Signin extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailError: '',
      passwordError: '',
      redirectToShoppingLists: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    // get handleLogin method from context api
    const { handleLogin } = this.context;
    this.handleLogin = handleLogin;
  }

  showPopUp = (title, message, redirect) => {
    confirmAlert({
      title,
      message,
      buttons: [
        {
          label: 'Ok',
          onClick: () => {
            this.setState({
              redirectToShoppingLists: redirect,
            });
          },
        },
      ],
    });
  };

  handleChange = (event) => {
    // make inputs controlled components
    const { id, value } = event.target;
    // error name for a particular field
    const error = `${id}Error`;
    if (value.length > 50) {
      this.setState({
        [id]: value,
        [error]: 'Maximum length of this field – 50 characters',
      });
    } else {
      this.setState({
        [id]: value,
        [error]: '',
      });
    }
  };

  async handleSubmit(event) {
    // do not refresh a page
    event.preventDefault();
    const { email, password, emailError, passwordError } = this.state;
    // if there is an error, do not sign in
    if (emailError || passwordError) {
      return;
    }
    const credentials = {
      username: email,
      password,
    };

    const response = await fetch('http://localhost:8000/auth/token/obtain', {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(credentials),
    });

    // if authenticated
    if (response.status === 200) {
      const data = await response.json();
      // save access token to local storage
      this.handleLogin(data.access);
      this.showPopUp('Success', 'Welcome!', true);
    } else {
      this.showPopUp('Error', 'Invalid email or password.', false);
    }
  }

  render() {
    const { email, emailError, password, passwordError, redirectToShoppingLists } = this.state;
    // if authenticated, redirect to list of lists
    if (redirectToShoppingLists) {
      return <Redirect to="/" />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <h1>Sign in</h1>
        <Input
          id="email"
          value={email}
          placeholder="Enter email"
          type="email"
          errorMessage={emailError}
          handleChange={this.handleChange}
        />
        <Input
          id="password"
          value={password}
          placeholder="Enter password"
          type="password"
          errorMessage={passwordError}
          handleChange={this.handleChange}
        />
        <Button text="Sign in" />
      </form>
    );
  }
}

export default Signin;
