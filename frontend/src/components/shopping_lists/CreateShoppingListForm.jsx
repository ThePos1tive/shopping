import React from 'react';
import { Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import Input from '../common/Input';
import Button from '../common/Button';
import AuthContext from '../../Context';

export default class CreateShoppingListForm extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      nameError: '',
      redirectToCreatedShoppingListView: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { handleLogout } = this.context;
    this.handleLogout = handleLogout;
  }

  handleChange = (event) => {
    const { value } = event.target;
    if (value.length > 30) {
      this.setState({
        name: value,
        nameError: 'Maximum length of this field – 30 characters',
      });
    } else {
      this.setState({
        name: value,
        nameError: '',
      });
    }
  };

  async handleSubmit(event) {
    event.preventDefault();
    const { name, nameError } = this.state;
    // if name is empty or there is an error, do nothing
    if (!name.trim() || nameError) {
      return;
    }
    const response = await fetch('http://localhost:8000/lists/', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('shopping_access_token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name: name.trim() }),
    });
    if (response.status === 201) {
      const data = await response.json();
      this.setState({
        name: '',
        createdShoppingListId: data.id,
        redirectToCreatedShoppingListView: true,
      });
    } else if (response.status === 401) {
      this.handleLogout();
    } else {
      confirmAlert({
        title: 'Error',
        message: 'Oops, looks like something went wrong',
        buttons: [{ label: 'Ok' }],
      });
    }
  }

  render() {
    const {
      name,
      nameError,
      redirectToCreatedShoppingListView,
      createdShoppingListId,
    } = this.state;
    if (redirectToCreatedShoppingListView && createdShoppingListId) {
      return <Redirect to={`/lists/${createdShoppingListId}`} />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <Input
          id={name}
          value={name}
          type="text"
          placeholder="Enter list name"
          handleChange={this.handleChange}
          errorMessage={nameError}
        />
        <Button text="Create a shopping list" />
      </form>
    );
  }
}
