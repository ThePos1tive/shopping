import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import RemoveButton from '../common/RemoveButton';

const ShoppingListItem = ({ id, text, handleShowConfirmationPopUp, isFinished }) => (
  <li className="list-group-item">
    <Link style={{ textDecoration: isFinished ? 'line-through' : null }} to={`/lists/${id}`}>{text}</Link>
    <RemoveButton handleShowConfirmationPopUp={() => handleShowConfirmationPopUp(id)} />
  </li>
);

ShoppingListItem.propTypes = {
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  isFinished: PropTypes.bool.isRequired,
  handleShowConfirmationPopUp: PropTypes.func.isRequired,
};

export default ShoppingListItem;
