import React from 'react';
import { Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import Spinner from '../common/Spinner';
import Button from '../common/Button';
import ShoppingListItem from './ShoppingListItem';
import CreateShoppingListForm from './CreateShoppingListForm';
import AuthContext from '../../Context';

export default class ListOfShoppingList extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      shoppingLists: [],
      isLoading: true,
      pagination: {
        numberOfPages: 0,
        currentPage: 1,
      },
    };

    this.loadShoppingLists = this.loadShoppingLists.bind(this);
    this.removeShoppingList = this.removeShoppingList.bind(this);
  }

  componentDidMount() {
    const { handleLogout } = this.context;
    this.handleLogout = handleLogout;
    const {
      pagination: { currentPage },
    } = this.state;
    this.loadShoppingLists(currentPage);
  }

  handleShowConfirmationPopUp = (listId) => {
    confirmAlert({
      title: 'Confirm',
      message: 'Are you sure to remove this list?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.removeShoppingList(listId),
        },
        {
          label: 'No',
        },
      ],
    });
  };

  rawShoppingListsToView = () => {
    const { shoppingLists } = this.state;
    const listItems = [];
    shoppingLists.forEach((item) => {
      listItems.push(
        <ShoppingListItem
          id={item.id}
          key={item.id}
          text={item.name}
          isFinished={item.is_finished}
          handleShowConfirmationPopUp={this.handleShowConfirmationPopUp}
        />,
      );
    });
    return listItems;
  };

  rawPaginationToView = () => {
    const {
      pagination: { numberOfPages, currentPage },
    } = this.state;

    const paginationButtons = [];
    for (let i = 1; i <= numberOfPages; i += 1) {
      paginationButtons.push(
        <li className="page-item px-1" key={i}>
          <Button
            type="button"
            // mark the current page button in a different color
            className={i === currentPage ? 'btn btn-raised active' : 'btn btn-raised btn-secondary'}
            text={i.toString()}
            // do not handle click on current page button
            onClick={i === currentPage ? null : () => this.loadShoppingLists(i)}
          />
        </li>,
      );
    }
    return paginationButtons;
  };

  async loadShoppingLists(pageNumber) {
    // by default load first page
    // load shopping lists
    const response = await fetch(`http://localhost:8000/lists/?page=${pageNumber}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('shopping_access_token')}`,
      },
    });
    if (response.status === 200) {
      const data = await response.json();
      // save lists, number of pages to the state
      this.setState({
        shoppingLists: data.results,
        isLoading: false,
        pagination: {
          numberOfPages: Math.ceil(data.count / 10),
          currentPage: pageNumber,
        },
      });
    } else if (response.status === 401) {
      this.handleLogout();
    }
  }

  async removeShoppingList(listId) {
    const {
      shoppingLists,
      pagination: { numberOfPages, currentPage },
    } = this.state;
    const response = await fetch(`http://localhost:8000/lists/${listId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('shopping_access_token')}`,
      },
    });

    if (response.status === 204) {
      // it happens only when the currentPage is invalid number
      // or the last page had only one item and the item was removed
      // and it trying to load the last page but it does not exist (any more)
      if (shoppingLists.length === 1 && numberOfPages === currentPage && numberOfPages > 1) {
        this.loadShoppingLists(currentPage - 1);
      } else {
        this.loadShoppingLists(currentPage);
      }
    } else if (response.status === 401) {
      this.handleLogout();
    }
  }

  render() {
    const { isAuthenticated } = this.context;
    const { isLoading } = this.state;
    if (!isAuthenticated) return <Redirect to="/signin" />;
    if (isLoading) return <Spinner />;
    const listItems = this.rawShoppingListsToView();
    const paginationButtons = this.rawPaginationToView();

    return (
      <div className="full-list">
        <h1 className="text-center">My shopping lists</h1>
        <br />
        <ul className="list-group">
          {listItems.length > 0 ? (
            listItems
          ) : (
            <h6 className="text-center">You do not have shopping lists</h6>
          )}
        </ul>
        <br />
        {paginationButtons.length > 1 && (
          <nav aria-label="Page navigation example">
            <ul className="pagination justify-content-center">
              {/* show pagination buttons when  there are more than 1 pages */}
              {paginationButtons}
            </ul>
          </nav>
        )}
        <CreateShoppingListForm />
      </div>
    );
  }
}
