import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import ErrorPage from '../common/Error';
import ShoppingListItem from './ShoppingItem';
import AddItemForm from './AddShoppingItemForm';
import Button from '../common/Button';
import Spinner from '../common/Spinner';
import AuthContext from '../../Context';

export default class ShoppingList extends React.Component {
  static contextType = AuthContext;

  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  };

  constructor(props) {
    super(props);
    // get shopping list id from url
    const {
      match: {
        params: { id },
      },
    } = props;

    // id - current shopping list id
    this.state = {
      id,
      isLoading: true,
      removedShoppingItemsIds: [],
      items: [],
      // newShoppingItems: [],
    };

    this.loadShoppingList = this.loadShoppingList.bind(this);
    this.handleSaveChanges = this.handleSaveChanges.bind(this);
  }

  componentDidMount() {
    const { handleLogout } = this.context;
    this.handleLogout = handleLogout;
    this.loadShoppingList();
  }

  handleAddShoppingItem = (newShoppingItem) => {
    // add new item to the existing
    const { items } = this.state;
    this.setState({
      items: [...items, newShoppingItem],
    });
  };

  handleRemoveShoppingItem = (itemIndexInItemsArray) => {
    // remove item from a state.items and add item.id to the state.removedShoppingItemsIds
    const { items, removedShoppingItemsIds } = this.state;
    // if item has an id, it means that this item is saved in database (only saved items have id)
    // and id of this item should be sent to the server to remove that item
    // if the item do not have an id, it means item is not saved in database (just was added)
    // can ignore sending to the server (simple remove form the state)
    if (items[itemIndexInItemsArray].id) {
      removedShoppingItemsIds.push(items[itemIndexInItemsArray].id);
      // remove only one item with index={itemIndexInItemsArray}
      items.splice(itemIndexInItemsArray, 1);
      this.setState({
        items,
        removedShoppingItemsIds,
      });
    } else {
      items.splice(itemIndexInItemsArray, 1);
      this.setState({
        items,
      });
    }
  };

  handleCheckboxChange = (event, index) => {
    // make all items controlled elements
    // 'index' is the index (in a list of items) of the changed item
    const { items } = this.state;
    items[index].is_done = event.target.checked;
    this.setState({
      items,
    });
  };

  showPopUp = (title, message) => {
    confirmAlert({
      title,
      message,
      buttons: [{ label: 'Ok' }],
    });
  };

  rawShoppingItemsToView = () => {
    // convert raw data to components
    const { items } = this.state;
    const itemsView = [];
    // this index allows to make checkbox controlled element
    // it is required for changing data in the state
    // so have the same data in a view and the state
    let index = 0;
    items.forEach((item) => {
      // this key helps react to determine if re-rendering is required
      // if item.id does not exist (it happens when new item has been added
      // and it is not synchronized with server (not in database yet))
      const key = item.id ? item.id : Math.floor(Math.random() * 12345);
      itemsView.push(
        <ShoppingListItem
          index={index}
          handleCheckboxChange={this.handleCheckboxChange}
          key={key}
          id={key}
          text={item.name}
          isChecked={item.is_done}
          handleRemoveShoppingItem={this.handleRemoveShoppingItem}
        />,
      );
      index += 1;
    });
    return itemsView;
  };

  async loadShoppingList() {
    // load a list (items and all properties), save it in the state
    const { id } = this.state;
    const response = await fetch(`http://localhost:8000/lists/${id}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('shopping_access_token')}`,
      },
    });
    // if current user does not have a list with id={id}
    if (response.status === 200) {
      // save list in the state
      const data = await response.json();
      this.setState({
        id: data.id,
        name: data.name,
        items: data.items,
        removedShoppingItemsIds: [],
        isLoading: false,
      });
    } else if (response.status === 401) {
      this.handleLogout();
    } else {
      // render error component (403) if there is no list with id={id}
      this.setState({
        isError: true,
        isLoading: false,
      });
    }
  }

  async handleSaveChanges() {
    const { id, items, removedShoppingItemsIds } = this.state;
    const response = await fetch(`http://localhost:8000/lists/${id}/`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('shopping_access_token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ items, removedItems: removedShoppingItemsIds }),
    });
    // show pop-up message
    if (response.status === 200) {
      this.showPopUp('Success', 'Changes have been saved.');
      this.loadShoppingList();
    } else if (response.status === 401) {
      this.handleLogout();
    } else {
      this.showPopUp('Error', 'Oops, looks like something went wrong.');
    }
  }

  render() {
    const { isAuthenticated } = this.context;
    const { isError, isLoading, name: shoppingListName } = this.state;
    if (!isAuthenticated) return <Redirect to="/signin" />;
    if (isLoading) return <Spinner />;
    if (isError) return <ErrorPage code={403} message="Sorry, you don’t have access to this page." />;
    const itemsView = this.rawShoppingItemsToView();

    return (
      <div className="full-list">
        <h1 className="text-center">{shoppingListName}</h1>
        <br />
        <ul className="list-group">
          {itemsView.length > 0 ? itemsView : <h6 className="text-center">This list is empty</h6>}
        </ul>
        <AddItemForm handleAddNewShoppingItem={this.handleAddShoppingItem} />
        <br />
        <div className="d-flex justify-content-between" style={{ padding: '0.6rem' }}>
          <Button
            type="button"
            onClick={this.loadShoppingList}
            className="btn btn-raised btn-danger"
            text="Cancel"
          />
          <Button text="Save changes" onClick={this.handleSaveChanges} />
        </div>
      </div>
    );
  }
}
