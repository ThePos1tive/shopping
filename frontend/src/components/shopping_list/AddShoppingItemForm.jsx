import React from 'react';
import PropTypes from 'prop-types';
import Input from '../common/Input';
import Button from '../common/Button';

export default class AddItemForm extends React.Component {
  static propTypes = {
    handleAddNewShoppingItem: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      nameError: '',
    };
  }

  handleChange = (event) => {
    const { value } = event.target;
    if (value.length > 30) {
      this.setState({
        name: value,
        nameError: 'Maximum length of this field – 30 characters',
      });
    } else {
      this.setState({
        name: value,
        nameError: '',
      });
    }
  };

  handleSubmit = (event) => {
    // do not refresh the page
    event.preventDefault();
    const { name, nameError } = this.state;
    const { handleAddNewShoppingItem } = this.props;
    // if name is empty or there is an error, do nothing
    if (!name.trim() || nameError) {
      return;
    }
    // lift-up new item to the list of shopping items
    const newShoppingItem = { name: name.trim(), is_done: false };
    handleAddNewShoppingItem(newShoppingItem);
    this.setState({
      name: '',
      nameError: '',
    });
  };

  render() {
    const { name, nameError } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <Input
          id="name"
          type="text"
          value={name}
          placeholder="Enter item name"
          handleChange={this.handleChange}
          errorMessage={nameError}
        />
        <Button text="Add shopping item" />
      </form>
    );
  }
}
