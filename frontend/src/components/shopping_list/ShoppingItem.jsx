import React from 'react';
import PropTypes from 'prop-types';
import RemoveButton from '../common/RemoveButton';

const ShoppingItem = ({
  id,
  text,
  isChecked,
  index,
  handleCheckboxChange,
  handleRemoveShoppingItem,
}) => (
  // 'id' param is required only for label + input
  // click on label changes input's checked
  <li className="list-group-item" key={id}>
    <div className="form-check">
      <input
        className="form-check-input"
        style={{ marginTop: '0px' }}
        type="checkbox"
        value=""
        id={id}
        checked={isChecked}
        onChange={event => handleCheckboxChange(event, index)}
      />
      <label
        className="form-check-label"
        style={{ textDecoration: isChecked ? 'line-through' : null }}
        htmlFor={id}
      >
        {text}
      </label>
    </div>
    <RemoveButton handleShowConfirmationPopUp={() => handleRemoveShoppingItem(index)} />
  </li>
);

ShoppingItem.propTypes = {
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  handleCheckboxChange: PropTypes.func.isRequired,
  handleRemoveShoppingItem: PropTypes.func.isRequired,
};

export default ShoppingItem;
