import React from 'react';

const AuthContext = React.createContext({ user: 'context user' });

export const AuthProvider = AuthContext.Provider;
export const AuthConsumer = AuthContext.Consumer;
export default AuthContext;
