# Shopping

Shopping is a web application that allows users to create and manage shopping lists.

## Getting Started

### Prerequisites

To work with this application you should install [Docker](https://www.docker.com/).

##### Mac OS: ```brew cask install docker```
##### Windows: download [here](https://docs.docker.com/docker-for-windows/install/).

### Installing
1. Open terminal in a project folder.
2. Run command ```docker-compose up --build```.
3. Run command ```make makemigrations```.
4. Run command ```make migrate```.
5. It works.

## Go to [MAIN PAGE](http://localhost:3000/) and use it.

## Built With
* [Django](https://www.djangoproject.com/) - Python web framework
* [React](https://reactjs.org/) - JavaScript UI library
* [Django REST framework](https://www.django-rest-framework.org/) - Powerful and flexible toolkit for building Web APIs
* [PostgreSQL](https://www.postgresql.org/) - Powerful, open source object-relational database system
